import GridEx from './components/GridEx'
import Todo from './components/Todo';
import './style.css';
import NavBar from './components/NavBar'
function App() {
  return (
    <div className="app">
      <NavBar />
      <Todo />
      <GridEx />
    </div>
  );
}

export default App;
