import React, { Component } from 'react'

export class AddTodo extends Component {
    constructor(props){
        super(props);
        this.state={
            task:'',
        }
    }
 
    render() {
        const {handler} = this.props;

     
        const handleSubmit=(e)=>{
            e.preventDefault();
            handler(this.state.task);
            this.setState({task:""});
        }
        return (
            <div>
                <form onSubmit={handleSubmit}>                
                <input type="text" name="todo" value={this.state.task}
                onChange={(e)=>this.setState({task:e.target.value})} placeholder="Todo what?"/>
                    <input type="submit" value="Add" />
                </form>

            </div>
        );
    }
}

export default AddTodo
