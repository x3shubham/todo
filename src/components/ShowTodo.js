import React, { Component } from 'react'
import Task from './Task'
export class ShowTodo extends Component {
    render() {
       let list=this.props.list;
        return (
            <div>
              {
                  list.map((item)=>{
                  return <Task name={item} key={item} deleteTask={this.props.del}/>
                  })
              }
            </div>
        )
    }
}

export default ShowTodo
