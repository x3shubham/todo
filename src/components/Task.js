import React, { Component } from 'react'

export class Task extends Component {

    handleClick = () => {
    let { deleteTask} = this.props;

        deleteTask(this.props.name);

   }
    render() {
        return (
            <div className="task-div">
                {this.props.name}
                <button onClick={this.handleClick}>
                    <i className="fas fa-trash"></i>
                </button>
            </div>
        )
    }
}

export default Task
