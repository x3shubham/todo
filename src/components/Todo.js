import React, { Component } from 'react'
import ShowTodo from './ShowTodo'
import AddTodo from './AddTodo'

export class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todoList: [],
        }
    }
    handleClick = (name) => {
        let todoList = [...this.state.todoList];
        todoList.push(name);
        this.setState({ todoList });
    }
    deleteTask = (name) => {
        let newTasks = [...this.state.todoList];
        console.log(newTasks + "inside todo list");
        let todoList = newTasks.filter((item) => {
            if ((item.toString() === name.toString())) {
                return;
            }
            return item;
        });
        this.setState({ todoList });

    }
    render() {
        return (
            <div className="todo-box">
                <AddTodo handler={this.handleClick} />
                <ShowTodo del={this.deleteTask} list={this.state.todoList} />

            </div>
        )
    }
}

export default Todo
